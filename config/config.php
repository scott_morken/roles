<?php

use Smorken\Roles\Constants\RoleName;

return [
    'layout' => 'layouts.app',
    'load_routes' => true,
    'allowed' => [
        [
            'code' => RoleName::SUPER_ADMIN->value,
            'descr' => RoleName::SUPER_ADMIN->label(),
            'level' => 1000,
        ],
        [
            'code' => RoleName::ADMIN->value,
            'descr' => RoleName::ADMIN->label(),
            'level' => 900,
        ],
        [
            'code' => RoleName::MANAGE->value,
            'descr' => RoleName::MANAGE->label(),
            'level' => 800,
        ],
        [
            'code' => RoleName::USER->value,
            'descr' => RoleName::USER->label(),
            'level' => 100,
        ],
        [
            'code' => RoleName::NONE->value,
            'descr' => RoleName::NONE->label(),
            'level' => 0,
        ],
    ],
    'models' => [
        \Smorken\Roles\Contracts\Models\Role::class => \Smorken\Roles\Models\Eloquent\Role::class,
        \Smorken\Roles\Contracts\Models\RoleUser::class => \Smorken\Roles\Models\Eloquent\RoleUser::class,
    ],
    'actions' => [
        \Smorken\Roles\Contracts\Actions\AddRoleUserForUserIdAction::class => \Smorken\Roles\Actions\AddRoleUserForUserIdAction::class,
        \Smorken\Roles\Contracts\Actions\DeleteRoleAction::class => \Smorken\Roles\Actions\DeleteRoleAction::class,
        \Smorken\Roles\Contracts\Actions\DeleteRoleUserAction::class => \Smorken\Roles\Actions\DeleteRoleUserAction::class,
        \Smorken\Roles\Contracts\Actions\DeleteRoleUserByUserIdAction::class => \Smorken\Roles\Actions\DeleteRoleUserByUserIdAction::class,
        \Smorken\Roles\Contracts\Actions\UpsertRoleAction::class => \Smorken\Roles\Actions\UpsertRoleAction::class,
        \Smorken\Roles\Contracts\Actions\UpsertRoleUserAction::class => \Smorken\Roles\Actions\UpsertRoleUserAction::class,
    ],
    'repositories' => [
        \Smorken\Roles\Contracts\Repositories\FilteredRolesRepository::class => \Smorken\Roles\Repositories\FilteredRolesRepository::class,
        \Smorken\Roles\Contracts\Repositories\FilteredRoleUsersRepository::class => \Smorken\Roles\Repositories\FilteredRoleUsersRepository::class,
        \Smorken\Roles\Contracts\Repositories\FindRoleByCodeRepository::class => \Smorken\Roles\Repositories\FindRoleByCodeRepository::class,
        \Smorken\Roles\Contracts\Repositories\FindRoleRepository::class => \Smorken\Roles\Repositories\FindRoleRepository::class,
        \Smorken\Roles\Contracts\Repositories\FindRoleUserRepository::class => \Smorken\Roles\Repositories\FindRoleUserRepository::class,
        \Smorken\Roles\Contracts\Repositories\FindRoleUserByUserIdRepository::class => \Smorken\Roles\Repositories\FindRoleUserByUserIdRepository::class,
        \Smorken\Roles\Contracts\Repositories\RolesRepository::class => \Smorken\Roles\Repositories\RolesRepository::class,
    ],
    'policies' => [
        \Smorken\Roles\Models\Eloquent\RoleUser::class => \Smorken\Roles\Policies\RoleUserPolicy::class,
    ],
];
