<?php

declare(strict_types=1);

namespace Smorken\Roles\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\FilteredRepository;

interface FilteredRolesRepository extends FilteredRepository {}
