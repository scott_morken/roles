<?php

declare(strict_types=1);

namespace Smorken\Roles\Contracts\Repositories;

use Smorken\Domain\Repositories\Contracts\RetrieveRepository;

interface FindRoleByCodeRepository extends RetrieveRepository {}
