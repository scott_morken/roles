<?php

namespace Smorken\Roles\Contracts;

use Illuminate\Support\Collection;

interface Role
{
    public function all(): Collection;

    public function currentRole(int $userId, bool $check = true): ?Models\Role;

    public function has(Models\Role $role, int $userId, bool $check = true): bool;

    public function hasCode(string $code, int $userId, bool $check = true): bool;

    public function hasId(int $id, int $userId, bool $check = true): bool;

    public function hasLevel(int $level, int $userId, bool $check = true): bool;

    public function max(): \Smorken\Roles\Contracts\Models\Role;

    public function resetUser(): void;
}
