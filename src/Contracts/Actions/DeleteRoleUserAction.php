<?php

declare(strict_types=1);

namespace Smorken\Roles\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\DeleteAction;

interface DeleteRoleUserAction extends DeleteAction {}
