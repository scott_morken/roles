<?php

declare(strict_types=1);

namespace Smorken\Roles\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;
use Smorken\Roles\Contracts\Models\RoleUser;

interface AddRoleUserForUserIdAction extends Action
{
    public function __invoke(int $userId, ?string $code = null): RoleUser;
}
