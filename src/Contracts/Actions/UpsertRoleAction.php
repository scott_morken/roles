<?php

declare(strict_types=1);

namespace Smorken\Roles\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\UpsertAction;

interface UpsertRoleAction extends UpsertAction {}
