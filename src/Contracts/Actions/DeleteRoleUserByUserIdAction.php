<?php

declare(strict_types=1);

namespace Smorken\Roles\Contracts\Actions;

use Smorken\Domain\Actions\Contracts\Action;

interface DeleteRoleUserByUserIdAction extends Action
{
    public function __invoke(int $userId): bool;
}
