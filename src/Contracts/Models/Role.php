<?php

namespace Smorken\Roles\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Role
 *
 *
 * @property int $id
 * @property int $level
 * @property string $code
 * @property string $descr
 * @property \Illuminate\Support\Collection $roleUsers
 *
 * @phpstan-require-extends \Smorken\Roles\Models\Eloquent\Role
 */
interface Role extends Model {}
