<?php

namespace Smorken\Roles\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface RoleUser
 *
 *
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property \Smorken\Roles\Contracts\Models\Role $role
 *
 * @phpstan-require-extends \Smorken\Roles\Models\Eloquent\RoleUser
 */
interface RoleUser extends Model {}
