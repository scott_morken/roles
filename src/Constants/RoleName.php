<?php

namespace Smorken\Roles\Constants;

use Smorken\Support\Constants\Attributes\Concerns\EnumHasLabel;
use Smorken\Support\Constants\Attributes\EnumLabel;

enum RoleName: string
{
    use EnumHasLabel;

    #[EnumLabel('Administrator')]
    case ADMIN = 'admin';
    #[EnumLabel('Manager')]
    case MANAGE = 'manage';
    #[EnumLabel('None')]
    case NONE = 'none';
    #[EnumLabel('Super Admin')]
    case SUPER_ADMIN = 'super-admin';
    #[EnumLabel('User')]
    case USER = 'user';

    public static function toString(string $key): string
    {
        return self::toArray()[$key];
    }

    public function toGateName(): string
    {
        return 'role-'.$this->value;
    }
}
