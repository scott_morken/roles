<?php

namespace Smorken\Roles;

use Illuminate\Support\Collection;
use Smorken\Roles\Factories\RoleRepositoryFactory;
use Smorken\Roles\Factories\RoleUserRepositoryFactory;

class Role implements \Smorken\Roles\Contracts\Role
{
    protected int $lastUserId = 0;

    protected ?Collection $roles = null;

    protected ?\Smorken\Roles\Contracts\Models\Role $usersRole = null;

    public function __construct(
        protected RoleRepositoryFactory $roleRepositoryFactory,
        protected RoleUserRepositoryFactory $roleUserRepositoryFactory
    ) {
        $this->roleUserRepositoryFactory->setAuthorizeForFind(false);
    }

    public function all(): Collection
    {
        return $this->getRoles();
    }

    public function currentRole(int $userId, bool $check = true): ?Contracts\Models\Role
    {
        return $this->getUserRole($userId, $check);
    }

    public function has(Contracts\Models\Role $role, int $userId, bool $check = true): bool
    {
        return $this->roleIsAllowed($this->currentRole($userId, $check), $role);
    }

    public function hasCode(string $code, int $userId, bool $check = true): bool
    {
        return $this->roleIsAllowed($this->currentRole($userId, $check), $this->getRoleByCode($code));
    }

    public function hasId(int $id, int $userId, bool $check = true): bool
    {
        return $this->roleIsAllowed($this->currentRole($userId, $check), $this->getRoleById($id));
    }

    public function hasLevel(int $level, int $userId, bool $check = true): bool
    {
        return $this->roleIsAllowed($this->currentRole($userId, $check), $this->getRoleByLevel($level));
    }

    public function max(): \Smorken\Roles\Contracts\Models\Role
    {
        return $this->all()->sortByDesc('level')->first();
    }

    public function resetUser(): void
    {
        $this->roleUserRepositoryFactory->resetForUserId($this->lastUserId);
        $this->lastUserId = 0;
        $this->usersRole = null;
    }

    protected function checkUserId(int $userId, bool $check): void
    {
        if ($check === false) {
            return;
        }
        if ($this->lastUserId === 0) {
            $this->lastUserId = $userId;
        }
        if ($this->lastUserId !== $userId) {
            throw Exception::userIdChanged($userId, $this->lastUserId);
        }
    }

    protected function getRoleByCode(string $code): Contracts\Models\Role
    {
        return $this->getRoles()->get($code) ?? throw Exception::roleNotFound($code);
    }

    protected function getRoleById(int $id): Contracts\Models\Role
    {
        foreach ($this->getRoles() as $role) {
            if ($role->id === $id) {
                return $role;
            }
        }

        throw Exception::roleNotFound($id);
    }

    protected function getRoleByLevel(int $level): Contracts\Models\Role
    {
        foreach ($this->getRoles() as $role) {
            if ($role->level === $level) {
                return $role;
            }
        }

        throw Exception::roleNotFound($level);
    }

    protected function getRoles(): Collection
    {
        if ($this->roles === null) {
            $this->roles = $this->roleRepositoryFactory->all()->keyBy('code');
        }

        return $this->roles;
    }

    protected function getUserRole(int $userId, bool $check): ?Contracts\Models\Role
    {
        $this->checkUserId($userId, $check);
        if ($this->usersRole === null || $check === false) {
            $userRole = $this->roleUserRepositoryFactory->findbyUserId($userId);
            $this->usersRole = $userRole?->role;
        }

        return $this->usersRole;
    }

    protected function roleIsAllowed(
        ?Contracts\Models\Role $usersRole,
        ?Contracts\Models\Role $compareRole
    ): bool {
        if (! $usersRole || ! $compareRole) {
            return false;
        }

        return $usersRole->level >= $compareRole->level;
    }
}
