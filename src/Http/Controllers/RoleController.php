<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:03 AM
 */

namespace Smorken\Roles\Http\Controllers;

use Illuminate\Http\Request;
use Smorken\Controller\Contracts\View\WithResource\WithActionFactory;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Roles\Factories\RoleActionFactory;
use Smorken\Roles\Factories\RoleRepositoryFactory;

class RoleController extends CrudController implements WithActionFactory, WithRepositoryFactory
{
    protected string $baseView = 'sm-roles::admin.role';

    public function __construct(
        RoleRepositoryFactory $repositoryFactory,
        RoleActionFactory $actionFactory
    ) {
        parent::__construct();
        $this->repositoryFactory = $repositoryFactory;
        $this->actionFactory = $actionFactory;
    }

    protected function getFilterFromRequest(Request $request): QueryStringFilter
    {
        return \Smorken\QueryStringFilter\QueryStringFilter::from($request)
            ->addKeyValue('page');
    }
}
