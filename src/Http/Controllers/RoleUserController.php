<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:03 AM
 */

namespace Smorken\Roles\Http\Controllers;

use Illuminate\Http\Request;
use Smorken\Controller\Contracts\View\WithResource\WithActionFactory;
use Smorken\Controller\Contracts\View\WithResource\WithRepositoryFactory;
use Smorken\Controller\View\WithResource\CrudController;
use Smorken\Domain\Actions\Contracts\Upsertable;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Roles\Factories\RoleUserActionFactory;
use Smorken\Roles\Factories\RoleUserRepositoryFactory;

class RoleUserController extends CrudController implements WithActionFactory, WithRepositoryFactory
{
    protected string $baseView = 'sm-roles::admin.role_user';

    public function __construct(
        RoleUserRepositoryFactory $repositoryFactory,
        RoleUserActionFactory $actionFactory
    ) {
        parent::__construct();
        $this->repositoryFactory = $repositoryFactory;
        $this->actionFactory = $actionFactory;
    }

    protected function getFilterFromRequest(Request $request): QueryStringFilter
    {
        return \Smorken\QueryStringFilter\QueryStringFilter::from($request)
            ->setFilters(['userId', 'roleId'])
            ->addKeyValue('page');
    }

    protected function getUpsertableForUpsert(Request $request, int|string|null $id): Upsertable
    {
        $id ??= ['user_id' => $request->input('user_id')];

        return new \Smorken\Domain\Actions\Upsertable(['role_id' => $request->input('role_id')], $id);
    }
}
