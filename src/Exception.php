<?php

namespace Smorken\Roles;

class Exception extends \Exception
{
    public static function roleNotFound(string|int $lookup): self
    {
        return new self("Role not found for '$lookup'");
    }

    public static function userIdChanged($userId, $lastUserId): self
    {
        return new self("User ID changed from '$lastUserId' to '$userId'");
    }
}
