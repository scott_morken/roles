<?php

declare(strict_types=1);

namespace Smorken\Roles\Policies;

use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Domain\Authorization\Policies\ModelBasedPolicy;
use Smorken\Model\Contracts\Model;
use Smorken\Roles\Contracts\Models\Role;
use Smorken\Roles\Contracts\Repositories\FindRoleRepository;
use Smorken\Roles\Models\Eloquent\UserStub;

class RoleUserPolicy extends ModelBasedPolicy
{
    public function __construct(
        protected FindRoleRepository $findRoleRepository
    ) {}

    protected function findRole(int $roleId): Role
    {
        return ($this->findRoleRepository)($roleId);
    }

    protected function isUserAllowedToCreate(Authenticatable $user, ?array $attributes = null): Response
    {
        $roleId = $attributes['role_id'] ?? 0;
        if ($this->isUserAllowedToSetRole($user, $roleId)) {
            return $this->allow();
        }

        return $this->deny();
    }

    protected function isUserAllowedToCreateOnUpsert(Authenticatable $user, Model $model): bool|Response
    {
        /** @var \Smorken\Roles\Contracts\Models\RoleUser $model */
        $roleId = $model->role_id ?? 0;
        if ($this->isUserAllowedToSetRole($user, $roleId)) {
            return $this->allow();
        }

        return $this->deny('You do not have permission to create a new record.');
    }

    protected function isUserAllowedToDestroy(Authenticatable $user, Model $model): Response
    {
        /** @var \Smorken\Roles\Contracts\Models\RoleUser $model */
        $roleId = (int) $model->role_id;
        if ($this->isUserAllowedToSetRole($user, $roleId)) {
            return $this->allow();
        }

        return $this->deny();
    }

    protected function isUserAllowedToSetRole(Authenticatable $user, int $roleId): bool
    {
        $user = UserStub::fromAuthenticatable($user);
        $userRoleLevel = $user->role?->level ?? 0;
        $role = $this->findRole($roleId);

        return $userRoleLevel >= $role->level;
    }

    protected function isUserAllowedToUpdate(Authenticatable $user, Model $model): Response
    {
        /** @var \Smorken\Roles\Contracts\Models\RoleUser $model */
        $roleId = (int) $model->role_id;
        if ($this->isUserAllowedToSetRole($user, $roleId)) {
            return $this->allow();
        }

        return $this->deny();
    }

    protected function isUserAllowedToView(Authenticatable $user, Model $model): Response
    {
        return $this->allow();
    }

    protected function isUserAllowedToViewAny(Authenticatable $user): Response
    {
        return $this->allow();
    }
}
