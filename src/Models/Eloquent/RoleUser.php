<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 2:10 PM
 */

namespace Smorken\Roles\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Eloquent;
use Smorken\Roles\Models\Builders\RoleUserBuilder;

class RoleUser extends Eloquent implements \Smorken\Roles\Contracts\Models\RoleUser
{
    /** @use HasBuilder<RoleUserBuilder<static>> */
    use HasBuilder;

    use HasFactory;

    protected static string $builder = RoleUserBuilder::class;

    protected $fillable = ['role_id', 'user_id'];

    protected $table = 'role_user';

    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }
}
