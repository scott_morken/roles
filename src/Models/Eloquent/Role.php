<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 1:47 PM
 */

namespace Smorken\Roles\Models\Eloquent;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\HasBuilder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Smorken\Model\Concerns\WithFriendlyKeys;
use Smorken\Model\Eloquent;
use Smorken\Roles\Models\Builders\RoleBuilder;

class Role extends Eloquent implements \Smorken\Roles\Contracts\Models\Role
{
    /** @use HasBuilder<RoleBuilder<static>> */
    use HasBuilder;

    use HasFactory, WithFriendlyKeys;

    protected static string $builder = RoleBuilder::class;

    protected $fillable = ['level', 'code', 'descr'];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'level' => 'Level',
        'descr' => 'Description',
        'code' => 'Code',
    ];

    public function __toString(): string
    {
        return sprintf('[%s] %s', $this->code, $this->descr);
    }

    public function code(): Attribute
    {
        return Attribute::make(
            set: fn ($value) => $this->attributes['code'] = Str::slug($value),
        );
    }

    public function roleUsers(): HasMany
    {
        return $this->hasMany(RoleUser::class);
    }
}
