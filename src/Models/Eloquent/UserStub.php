<?php

declare(strict_types=1);

namespace Smorken\Roles\Models\Eloquent;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Smorken\Model\Contracts\Model;
use Smorken\Model\Eloquent;

class UserStub extends Eloquent implements Authenticatable
{
    public $incrementing = false;

    protected $fillable = ['id'];

    public static function fromAuthenticatable(Authenticatable $auth): Model|Authenticatable
    {
        if (method_exists($auth, 'role')) {
            return $auth;
        }

        return new self(['id' => $auth->getAuthIdentifier()]);
    }

    public function getAuthIdentifier(): int|string
    {
        return $this->id;
    }

    public function getAuthIdentifierName(): string
    {
        return 'id';
    }

    public function getAuthPassword(): string
    {
        return '';
    }

    public function getRememberToken(): string
    {
        return '';
    }

    public function getRememberTokenName(): string
    {
        return 'token';
    }

    public function role(): HasOneThrough
    {
        return $this->hasOneThrough(
            \Smorken\Roles\Models\Eloquent\Role::class,
            RoleUser::class,
            'user_id',
            'id',
            'id',
            'role_id'
        );
    }

    public function setRememberToken($value): void {}

    public function getAuthPasswordName()
    {
        return 'password';
    }
}
