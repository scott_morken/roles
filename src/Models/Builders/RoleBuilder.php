<?php

declare(strict_types=1);

namespace Smorken\Roles\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

/**
 * @template TModel of \Smorken\Roles\Models\Eloquent\Role
 *
 * @extends Builder<TModel>
 */
class RoleBuilder extends Builder
{
    use WithQueryStringFilter;

    /**
     * @return $this
     */
    public function codeIs(string $code): EloquentBuilder
    {
        /** @var $this */
        return $this->where('code', '=', $code);
    }

    /**
     * @return $this
     */
    public function defaultOrder(): EloquentBuilder
    {
        /** @var $this */
        return $this->orderBy('level', 'desc');
    }

    /**
     * @return $this
     */
    public function greaterThanEqual(int $level): EloquentBuilder
    {
        /** @var $this */
        return $this->where('level', '>=', $level);
    }

    /**
     * @return $this
     */
    public function lessThanEqual(int $level): EloquentBuilder
    {
        /** @var $this */
        return $this->where('level', '<=', $level);
    }
}
