<?php

declare(strict_types=1);

namespace Smorken\Roles\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

/**
 * @template TModel of \Smorken\Roles\Models\Eloquent\RoleUser
 *
 * @extends Builder<TModel>
 */
class RoleUserBuilder extends Builder
{
    use WithQueryStringFilter;

    /**
     * @return $this
     */
    public function defaultWiths(): EloquentBuilder
    {
        /** @var $this */
        return $this->with(['role']);
    }

    /**
     * @return $this
     */
    public function roleIdIs(int $roleId): EloquentBuilder
    {
        /** @var $this */
        return $this->where('role_id', '=', $roleId);
    }

    /**
     * @return $this
     */
    public function userIdIs(int $userId): EloquentBuilder
    {
        /** @var $this */
        return $this->where('user_id', '=', $userId);
    }

    protected function getFilterHandlersForFilters(): array
    {
        return [
            new FilterHandler('roleId', 'roleIdIs'),
            new FilterHandler('userId', 'userIdIs'),
        ];
    }
}
