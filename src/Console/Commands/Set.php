<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 12:06 PM
 */

namespace Smorken\Roles\Console\Commands;

use Illuminate\Console\Command;
use Smorken\Roles\Contracts\Actions\AddRoleUserForUserIdAction;

class Set extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Registers a user to a role (default max: super-admin)';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role:set {user_id : User ID} 
    {role? : Override the role, default is max}
    ';

    public function __construct(
        protected AddRoleUserForUserIdAction $addAction,
    ) {
        parent::__construct();
    }

    public function handle(): int
    {
        $userId = (int) $this->argument('user_id');
        $roleCode = $this->argument('role');
        $userRole = ($this->addAction)($userId, $roleCode);
        $this->info("User [$userId] added to role '{$userRole->role->descr}'.");

        return 0;
    }
}
