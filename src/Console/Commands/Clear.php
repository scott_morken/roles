<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 12:06 PM
 */

namespace Smorken\Roles\Console\Commands;

use Illuminate\Console\Command;
use Smorken\Roles\Contracts\Actions\DeleteRoleUserByUserIdAction;

class Clear extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear a users roles';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role:clear {user_id : User ID}';

    public function __construct(
        protected DeleteRoleUserByUserIdAction $deleteAction
    ) {
        parent::__construct();
    }

    public function handle(): int
    {
        $user_id = (int) $this->argument('user_id');
        if (($this->deleteAction)($user_id)) {
            $this->info("User [$user_id] has been removed from the access table.");

            return 0;
        }
        $this->info("User [$user_id] does not exist or was not removed.");

        return 1;
    }
}
