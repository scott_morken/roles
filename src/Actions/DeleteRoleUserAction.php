<?php

declare(strict_types=1);

namespace Smorken\Roles\Actions;

use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\Roles\Contracts\Models\RoleUser;

/**
 * @extends EloquentDeleteAction<\Smorken\Roles\Models\Eloquent\RoleUser>
 */
class DeleteRoleUserAction extends EloquentDeleteAction implements \Smorken\Roles\Contracts\Actions\DeleteRoleUserAction
{
    public function __construct(RoleUser $model)
    {
        parent::__construct($model);
    }
}
