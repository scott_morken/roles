<?php

declare(strict_types=1);

namespace Smorken\Roles\Actions;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Roles\Contracts\Models\RoleUser;

/**
 * @extends EloquentUpsertAction<\Smorken\Roles\Models\Eloquent\RoleUser, \Smorken\Domain\Actions\Upsertable>
 */
class UpsertRoleUserAction extends EloquentUpsertAction implements \Smorken\Roles\Contracts\Actions\UpsertRoleUserAction
{
    protected bool $includeIdentifierOnCreate = true;

    public function __construct(RoleUser $model)
    {
        parent::__construct($model);
    }

    /**
     * @template TBuilder of \Smorken\Roles\Models\Builders\RoleUserBuilder
     *
     * @param  TBuilder  $query
     */
    protected function findByIdUsingQuery(Builder $query, string|int|array $id, bool $throw): ?RoleUser
    {
        if (is_array($id) && isset($id['user_id'])) {
            $query = $query->userIdIs((int) $id['user_id']);

            /** @var \Smorken\Roles\Models\Eloquent\RoleUser|null */
            return $throw ? $query->firstOrFail() : $query->first();
        }

        /** @var \Smorken\Roles\Models\Eloquent\RoleUser|null */
        return $this->queryByFind($query, $id, $throw);
    }
}
