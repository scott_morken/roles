<?php

declare(strict_types=1);

namespace Smorken\Roles\Actions;

use Smorken\Domain\Actions\EloquentDeleteAction;
use Smorken\Roles\Contracts\Models\Role;

/**
 * @extends EloquentDeleteAction<\Smorken\Roles\Models\Eloquent\Role>
 */
class DeleteRoleAction extends EloquentDeleteAction implements \Smorken\Roles\Contracts\Actions\DeleteRoleAction
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
}
