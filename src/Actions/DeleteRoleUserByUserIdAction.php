<?php

declare(strict_types=1);

namespace Smorken\Roles\Actions;

use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\Roles\Contracts\Models\RoleUser;

/**
 * @extends ActionWithEloquent<\Smorken\Roles\Models\Eloquent\RoleUser>
 */
class DeleteRoleUserByUserIdAction extends ActionWithEloquent implements \Smorken\Roles\Contracts\Actions\DeleteRoleUserByUserIdAction
{
    public function __construct(RoleUser $model)
    {
        parent::__construct($model);
    }

    public function __invoke(int $userId): bool
    {
        return $this->eloquentModel()->newQuery()->userIdIs($userId)->delete();
    }
}
