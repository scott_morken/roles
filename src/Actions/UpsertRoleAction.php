<?php

declare(strict_types=1);

namespace Smorken\Roles\Actions;

use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Roles\Contracts\Models\Role;
use Smorken\Roles\Validation\RuleProviders\RoleRules;

/**
 * @extends EloquentUpsertAction<\Smorken\Roles\Models\Eloquent\Role, \Smorken\Domain\Actions\Upsertable>
 */
class UpsertRoleAction extends EloquentUpsertAction implements \Smorken\Roles\Contracts\Actions\UpsertRoleAction
{
    public $rulesProvider = RoleRules::class;

    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
}
