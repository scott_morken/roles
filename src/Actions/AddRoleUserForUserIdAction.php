<?php

declare(strict_types=1);

namespace Smorken\Roles\Actions;

use Smorken\Domain\Actions\ActionWithEloquent;
use Smorken\Roles\Contracts\Models\RoleUser;
use Smorken\Roles\Factories\RoleRepositoryFactory;

/**
 * @extends ActionWithEloquent<\Smorken\Roles\Models\Eloquent\RoleUser>
 */
class AddRoleUserForUserIdAction extends ActionWithEloquent implements \Smorken\Roles\Contracts\Actions\AddRoleUserForUserIdAction
{
    public function __construct(
        RoleUser $model,
        protected RoleRepositoryFactory $roleRepositoryFactory
    ) {
        parent::__construct($model);
    }

    public function __invoke(int $userId, ?string $code = null): RoleUser
    {
        $roleId = $this->getRoleId($code);
        /** @var RoleUser $m */
        $m = $this->eloquentModel()->newQuery()->userIdIs($userId)->first();
        if (! $m) {
            $m = $this->eloquentModel()->newInstance(['user_id' => $userId, 'role_id' => $roleId]);
        }
        $m->role_id = $roleId;
        $m->save();

        return $m;
    }

    protected function getRoleId(?string $code): int
    {
        if ($code) {
            return $this->roleRepositoryFactory->findByCode($code)->id;
        }

        return $this->roleRepositoryFactory->max()->id;
    }
}
