<?php

declare(strict_types=1);

namespace Smorken\Roles\Factories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Roles\Contracts\Models\RoleUser;
use Smorken\Roles\Contracts\Repositories\FilteredRoleUsersRepository;
use Smorken\Roles\Contracts\Repositories\FindRoleUserByUserIdRepository;
use Smorken\Roles\Contracts\Repositories\FindRoleUserRepository;
use Smorken\Support\Contracts\Filter;

class RoleUserRepositoryFactory extends RepositoryFactory
{
    protected array $handlers = [
        'find' => FindRoleUserRepository::class,
        'filtered' => FilteredRoleUsersRepository::class,
    ];

    public function emptyModel(): mixed
    {
        return $this->handlerForEmptyModel();
    }

    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): Paginator|Collection|iterable {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(mixed $id, bool $throw = true): mixed
    {
        $id = (int) $id;

        return $this->handlerForFind($id, $throw);
    }

    public function findbyUserId(int $userId): ?RoleUser
    {
        return $this->forRetrieve(FindRoleUserByUserIdRepository::class, $userId, false);
    }

    public function resetForUserId(int $userId): void
    {
        $r = $this->make(FindRoleUserByUserIdRepository::class);
        $r->setCacheKey($userId);
        $r->reset();
    }

    public function setAuthorizeForFind(bool $authorize): void
    {
        $r = $this->make(FindRoleUserByUserIdRepository::class);
        // TODO fix in Domain package
        // @phpstan-ignore method.notFound
        $r->setShouldAuthorize($authorize);
    }
}
