<?php

declare(strict_types=1);

namespace Smorken\Roles\Factories;

use Smorken\Domain\Factories\ActionFactory;
use Smorken\Roles\Contracts\Actions\DeleteRoleUserAction;
use Smorken\Roles\Contracts\Actions\UpsertRoleUserAction;

class RoleUserActionFactory extends ActionFactory
{
    protected array $handlers = [
        'delete' => DeleteRoleUserAction::class,
        'upsert' => UpsertRoleUserAction::class,
    ];
}
