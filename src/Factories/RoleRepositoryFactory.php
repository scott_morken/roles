<?php

declare(strict_types=1);

namespace Smorken\Roles\Factories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Smorken\Domain\Factories\RepositoryFactory;
use Smorken\QueryStringFilter\Contracts\QueryStringFilter;
use Smorken\Roles\Contracts\Models\Role;
use Smorken\Roles\Contracts\Repositories\FilteredRolesRepository;
use Smorken\Roles\Contracts\Repositories\FindRoleByCodeRepository;
use Smorken\Roles\Contracts\Repositories\FindRoleRepository;
use Smorken\Roles\Contracts\Repositories\RolesRepository;
use Smorken\Support\Contracts\Filter;

class RoleRepositoryFactory extends RepositoryFactory
{
    protected array $handlers = [
        'all' => RolesRepository::class,
        'find' => FindRoleRepository::class,
        'filtered' => FilteredRolesRepository::class,
    ];

    public function all(): Collection
    {
        return $this->handlerForAll();
    }

    public function emptyModel(): Role
    {
        return $this->handlerForEmptyModel();
    }

    public function filtered(
        QueryStringFilter|Filter $filter,
        int $perPage = 20
    ): Paginator|Collection {
        return $this->handlerForFiltered($filter, $perPage);
    }

    public function find(string|int $id, bool $throw = true): ?Role
    {
        $id = (int) $id;

        return $this->handlerForFind($id, $throw);
    }

    public function findByCode(string $code, bool $throw = true): ?Role
    {
        return $this->forRetrieve(FindRoleByCodeRepository::class, $code, $throw);
    }

    public function max(): Role
    {
        return $this->all()->sortByDesc('level')->first();
    }
}
