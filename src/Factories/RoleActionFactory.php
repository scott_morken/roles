<?php

declare(strict_types=1);

namespace Smorken\Roles\Factories;

use Smorken\Domain\Factories\ActionFactory;
use Smorken\Roles\Contracts\Actions\DeleteRoleAction;
use Smorken\Roles\Contracts\Actions\UpsertRoleAction;

class RoleActionFactory extends ActionFactory
{
    protected array $handlers = [
        'delete' => DeleteRoleAction::class,
        'upsert' => UpsertRoleAction::class,
    ];
}
