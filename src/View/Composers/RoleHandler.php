<?php

declare(strict_types=1);

namespace Smorken\Roles\View\Composers;

use Illuminate\Contracts\View\View;
use Smorken\Roles\Contracts\Role;

class RoleHandler
{
    public function __construct(protected Role $role) {}

    public function compose(View $view): void
    {
        $view->with('roleHandler', $this->role);
    }
}
