<?php

declare(strict_types=1);

namespace Smorken\Roles\View\Composers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\View\View;
use Smorken\Roles\Contracts\Role;

class AllowedRoles
{
    public function __construct(protected Role $role, protected Guard $guard) {}

    public function compose(View $view): void
    {
        $userRoleLevel = $this->guard->user()->role?->level ?? 0;
        $view->with('roles',
            $this->role->all()->filter(fn (\Smorken\Roles\Contracts\Models\Role $r) => $r->level <= $userRoleLevel));
    }
}
