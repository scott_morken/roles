<?php

declare(strict_types=1);

namespace Smorken\Roles\Validation\RuleProviders;

class RoleRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'code' => 'required|string|max:16',
            'descr' => 'required|string|max:32',
            'level' => 'required|integer|min:0|max:9999',
        ];
    }
}
