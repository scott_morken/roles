<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:04 AM
 */

namespace Smorken\Roles;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\View;
use Smorken\Domain\Authorization\SelfDefiningPolicyBuilder;
use Smorken\Roles\Console\Commands\Clear;
use Smorken\Roles\Console\Commands\Set;
use Smorken\Roles\Contracts\Role;
use Smorken\Roles\Factories\RoleRepositoryFactory;
use Smorken\Roles\Factories\RoleUserRepositoryFactory;
use Smorken\Roles\Support\DefineGates;
use Smorken\Roles\View\Composers\AllowedRoles;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public static function defineGates(Application $app, bool $force = false): void
    {
        DefineGates::define($app, $force);
    }

    public static function resetGated(): void
    {
        DefineGates::reset();
    }

    public function boot(): void
    {
        $this->loadConfig();
        $this->loadViews();
        $this->loadViewComposers();
        $this->loadRoutes();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->registerModels();
        $this->registerActions();
        $this->registerRepositories();
        $this->registerPolicies();
        $this->loadCommands();
        self::defineGates($this->app);
    }

    public function register(): void
    {
        $this->registerRoleHandler();
    }

    protected function loadCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Set::class,
                Clear::class,
            ]);
        }
    }

    protected function loadConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'sm-roles');
        $this->publishes([$config => config_path('sm-roles.php')], 'config');
    }

    protected function loadRoutes(): void
    {
        if ($this->app['config']->get('sm-roles.load_routes', false)) {
            $this->loadRoutesFrom(__DIR__.'/../routes/admin.php');
        }
    }

    protected function loadViewComposers(): void
    {
        View::composer([
            'sm-roles::admin.role_user._filter_form',
        ], AllowedRoles::class);
        View::composer([
            'sm-roles::admin.role_user._form',
        ], AllowedRoles::class);
    }

    protected function loadViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'sm-roles');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('/views/vendor/smorken/roles'),
            ],
            'views'
        );
    }

    protected function registerActions(): void
    {
        foreach ($this->app['config']->get('sm-roles.actions', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerModels(): void
    {
        foreach ($this->app['config']->get('sm-roles.models', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerPolicies(): void
    {
        $builder = new SelfDefiningPolicyBuilder($this->app[Gate::class], $this->app);
        $builder->build($this->app['config']->get('sm-roles.policies', []));
    }

    protected function registerRepositories(): void
    {
        foreach ($this->app['config']->get('sm-roles.repositories', []) as $contract => $impl) {
            $this->app->scoped($contract, static fn (Application $app) => $app[$impl]);
        }
    }

    protected function registerRoleHandler(): void
    {
        $this->app->scoped(
            Role::class,
            static fn (Application $app) => new \Smorken\Roles\Role(
                $app[RoleRepositoryFactory::class],
                $app[RoleUserRepositoryFactory::class]
            )
        );
    }
}
