<?php

declare(strict_types=1);

namespace Smorken\Roles\Support;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Smorken\Roles\Contracts\Role;

class DefineGates
{
    protected static ?RoleAbilities $roleAbilities = null;

    public static function define(Application $app, bool $force = false): void
    {
        if (! self::shouldContinue($app, $force)) {
            return;
        }
        /** @var \Illuminate\Contracts\Auth\Access\Gate $gate */
        $gate = $app->make(Gate::class);
        if (self::$roleAbilities) {
            (self::$roleAbilities)($gate);

            return;
        }
        try {
            /** @var \Smorken\Roles\Contracts\Role $handler */
            $handler = $app->make(Role::class);
            $roleAbilities = new RoleAbilities($handler);
            $roleAbilities($gate);
            self::$roleAbilities = $roleAbilities;
        } catch (\Throwable $e) {
            self::reportException($app, $e);
        }
    }

    public static function reset(bool $everything = false): void
    {
        if ($everything) {
            self::$roleAbilities = null;
        }
        if (! self::$roleAbilities) {
            return;
        }
        self::$roleAbilities->resetCached();
    }

    protected static function reportException(Application $app, \Throwable $e): void
    {
        try {
            $app->make(ExceptionHandler::class)->report($e);
        } catch (\Throwable) {
            // do nothing
        }
    }

    protected static function shouldContinue(Application $app, bool $force): bool
    {
        if ($force) {
            return true;
        }

        return ! $app->runningInConsole();
    }
}
