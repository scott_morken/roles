<?php

declare(strict_types=1);

namespace Smorken\Roles\Support;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\Collection;
use Smorken\Roles\Contracts\Role;

class RoleAbilities
{
    protected array $cached = [];

    protected ?Collection $roles = null;

    public function __construct(protected Role $roleHandler) {}

    public function __invoke(Gate $gate): void
    {
        foreach ($this->getRoles() as $role) {
            $ability = 'role-'.$role->code;
            if ($gate->has($ability)) {
                continue;
            }
            $gate->define($ability, function ($user) use ($role) {
                $userId = (int) $user->id;
                if (! $userId) {
                    return false;
                }
                $cached = $this->fromCache($userId, $role);
                if ($cached !== null) {
                    return $cached;
                }

                return $this->toCache($userId, $role, $this->roleHandler->hasCode($role->code, $userId));
            });
        }
    }

    public function resetCached(): void
    {
        $this->roleHandler->resetUser();
        $this->cached = [];
    }

    protected function fromCache(int $userId, \Smorken\Roles\Contracts\Models\Role $role): ?bool
    {
        return $this->cached[$role->code][$userId] ?? null;
    }

    /**
     * @return \Illuminate\Support\Collection<\Smorken\Roles\Contracts\Models\Role>
     */
    protected function getRoles(): Collection
    {
        return $this->roles ?? $this->roles = $this->roleHandler->all();
    }

    protected function toCache(int $userId, \Smorken\Roles\Contracts\Models\Role $role, bool $result): bool
    {
        $this->cached[$role->code][$userId] = $result;

        return $result;
    }
}
