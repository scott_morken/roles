<?php

declare(strict_types=1);

namespace Smorken\Roles\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Roles\Contracts\Models\RoleUser;

/**
 * @extends EloquentRetrieveRepository<\Smorken\Roles\Models\Eloquent\RoleUser>
 */
class FindRoleUserByUserIdRepository extends EloquentRetrieveRepository implements \Smorken\Roles\Contracts\Repositories\FindRoleUserByUserIdRepository
{
    public function __construct(RoleUser $model)
    {
        parent::__construct($model);
    }

    protected function findByIdUsingQuery(Builder $query, string|int|array $id, bool $throw): ?Model
    {
        // @phpstan-ignore method.notFound
        $query = $query->userIdIs($id);

        return $throw ? $query->firstOrFail() : $query->first();
    }
}
