<?php

declare(strict_types=1);

namespace Smorken\Roles\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\Roles\Contracts\Models\RoleUser;

/**
 * @extends EloquentFilteredRepository<\Smorken\Roles\Models\Eloquent\RoleUser>
 */
class FilteredRoleUsersRepository extends EloquentFilteredRepository implements \Smorken\Roles\Contracts\Repositories\FilteredRoleUsersRepository
{
    public function __construct(RoleUser $model)
    {
        parent::__construct($model);
    }
}
