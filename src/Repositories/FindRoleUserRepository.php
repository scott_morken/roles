<?php

declare(strict_types=1);

namespace Smorken\Roles\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Roles\Contracts\Models\RoleUser;

/**
 * @extends EloquentRetrieveRepository<\Smorken\Roles\Models\Eloquent\RoleUser>
 */
class FindRoleUserRepository extends EloquentRetrieveRepository implements \Smorken\Roles\Contracts\Repositories\FindRoleUserRepository
{
    public function __construct(RoleUser $model)
    {
        parent::__construct($model);
    }
}
