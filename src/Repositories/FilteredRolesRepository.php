<?php

declare(strict_types=1);

namespace Smorken\Roles\Repositories;

use Smorken\Domain\Repositories\EloquentFilteredRepository;
use Smorken\Roles\Contracts\Models\Role;

/**
 * @extends EloquentFilteredRepository<\Smorken\Roles\Models\Eloquent\Role>
 */
class FilteredRolesRepository extends EloquentFilteredRepository implements \Smorken\Roles\Contracts\Repositories\FilteredRolesRepository
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
}
