<?php

declare(strict_types=1);

namespace Smorken\Roles\Repositories;

use Smorken\Domain\Repositories\EloquentIterableRepository;
use Smorken\Roles\Contracts\Models\Role;

/**
 * @extends EloquentIterableRepository<\Smorken\Roles\Models\Eloquent\Role>
 */
class RolesRepository extends EloquentIterableRepository implements \Smorken\Roles\Contracts\Repositories\RolesRepository
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
}
