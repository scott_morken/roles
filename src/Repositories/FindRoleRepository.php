<?php

declare(strict_types=1);

namespace Smorken\Roles\Repositories;

use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Roles\Contracts\Models\Role;

/**
 * @extends EloquentRetrieveRepository<\Smorken\Roles\Models\Eloquent\Role>
 */
class FindRoleRepository extends EloquentRetrieveRepository implements \Smorken\Roles\Contracts\Repositories\FindRoleRepository
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }
}
