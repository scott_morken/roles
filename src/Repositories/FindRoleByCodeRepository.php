<?php

declare(strict_types=1);

namespace Smorken\Roles\Repositories;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Domain\Repositories\EloquentRetrieveRepository;
use Smorken\Roles\Contracts\Models\Role;

/**
 * @extends EloquentRetrieveRepository<\Smorken\Roles\Models\Eloquent\Role>
 */
class FindRoleByCodeRepository extends EloquentRetrieveRepository implements \Smorken\Roles\Contracts\Repositories\FindRoleByCodeRepository
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    /**
     * @template TBuilder of \Smorken\Roles\Models\Builders\RoleBuilder
     *
     * @param  TBuilder  $query
     */
    protected function findByIdUsingQuery(Builder $query, string|int|array $id, bool $throw): ?Role
    {
        $query = $query->codeIs($id);

        /** @var \Smorken\Roles\Models\Eloquent\Role|null */
        return $throw ? $query->firstOrFail() : $query->first();
    }
}
