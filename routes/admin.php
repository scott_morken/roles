<?php

declare(strict_types=1);
use Illuminate\Support\Facades\Route;
use Smorken\Roles\Http\Controllers\RoleController;
use Smorken\Roles\Http\Controllers\RoleUserController;

Route::middleware(['web', 'auth', 'can:role-admin'])
    ->prefix('admin')
    ->group(function () {
        Route::get('role/{id}/confirm-delete', [RoleController::class, 'confirmDelete']);
        Route::resource('role', RoleController::class)
            ->parameters(['role' => 'id']);

        Route::get('role-user/{id}/confirm-delete', [RoleUserController::class, 'confirmDelete']);
        Route::resource('role-user', RoleUserController::class)
            ->parameters(['role-user' => 'id']);
    });
