<?php
use Smorken\Components\Helpers\Model;

/**
 * @var \Smorken\Roles\Contracts\Models\Role $model
 * @var \Illuminate\Support\Collection<\Smorken\Roles\Contracts\Models\Role> $roles
 */
$rolesArray = $roles?->pluck('descr', 'id')->all() ?? [];
?>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'user_id'))
    <x-smc::input.label :model="$m">User ID</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'role_id'))
    <x-smc::input.label :model="$m">Role</x-smc::input.label>
    <x-smc::input.select-options :model="$m"
                                 :options="$rolesArray"
    />
</x-smc::input.group>


