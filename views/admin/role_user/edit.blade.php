@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-roles.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.edit
        title="Users Roles Administration"
        form-view="sm-roles::admin.role_user._form"
        :viewModel="$viewModel"></x-smc::resource.edit>
</x-dynamic-component>
