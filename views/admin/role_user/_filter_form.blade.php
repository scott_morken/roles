<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\FormQsFilter $helper
 * @var \Illuminate\Support\Collection<\Smorken\Roles\Contracts\Models\Role> $roles
 */
$rolesArray = $roles?->pluck('descr', 'id')->all() ?? [];
?>
<x-smc::form.qs-filter :filter="$filter">
    @php($helper = $component->helper)
    <x-smc::flex class="justify-content-start">
        <x-smc::input.group>
            <x-smc::input.label for="filter-roleid" :visible="false">Role</x-smc::input.label>
            <x-smc::input.select-options name="filter[roleId]" class="{{ $helper->getClasses('roleId') }}"
                                         :value="$helper->getValue('roleId')"
                                         :options="['' => '-- Any Role --'] + $rolesArray"
            />
        </x-smc::input.group>
        <x-smc::form.filter-buttons></x-smc::form.filter-buttons>
    </x-smc::flex>
</x-smc::form.qs-filter>
