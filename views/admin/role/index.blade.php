@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-roles.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.index-table
            title="Roles Administration"
            formFilterView="sm-roles::admin.role._filter_form"
            :viewModel="$viewModel"
    ></x-smc::resource.index-table>
</x-dynamic-component>
