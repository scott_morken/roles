<?php
/**
 * @var \Smorken\Components\Contracts\Helpers\FormQsFilter $helper
 */
?>
<x-smc::form.qs-filter :filter="$filter">
    @php($helper = $component->helper)
    <x-smc::flex class="justify-content-start">

    </x-smc::flex>
</x-smc::form.qs-filter>
