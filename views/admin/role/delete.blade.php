@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-roles.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.confirm-delete title="Roles Administration"
                          :viewModel="$viewModel"></x-smc::resource.confirm-delete>
</x-dynamic-component>
