@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-roles.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.create
        title="Roles Administration"
        form-view="sm-roles::admin.role._form"
        :viewModel="$viewModel"></x-smc::resource.create>
</x-dynamic-component>
