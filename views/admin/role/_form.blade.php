<?php
use Smorken\Components\Helpers\Model;

/**
 * @var \Smorken\Roles\Contracts\Models\Role $model
 */
?>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'level'))
    <x-smc::input.label :model="$m">Level</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'code'))
    <x-smc::input.label :model="$m">Code</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>
<x-smc::input.group>
    @php($m = Model::newInstance($model, 'descr'))
    <x-smc::input.label :model="$m">Description</x-smc::input.label>
    <x-smc::input :model="$m"></x-smc::input>
</x-smc::input.group>


