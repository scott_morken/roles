@php($layoutComponent = \Illuminate\Support\Facades\Config::get('sm-roles.layout', 'layouts.app'))
<x-dynamic-component :component="$layoutComponent">
    <x-smc::resource.show-rows title="Roles Administration"
                          :viewModel="$viewModel"></x-smc::resource.show-rows>
</x-dynamic-component>
