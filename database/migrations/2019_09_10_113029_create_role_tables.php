<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    protected array $tables = [
        'roles',
        'role_user',
    ];

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        foreach ($this->tables as $table) {
            Schema::dropIfExists($table);
        }
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $this->roles();
        $this->roleUser();
    }

    protected function createRoleUser(): void
    {
        Schema::create('role_user', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')
                ->unsigned();
            $table->bigInteger('role_id')
                ->unsigned();
            $table->timestamps();

            $table->unique('user_id');
            $table->index('role_id');
        });
    }

    protected function roleUser(): void
    {
        $this->createRoleUser();
    }

    protected function roles(): void
    {
        Schema::create('roles', function (Blueprint $t) {
            $t->increments('id');
            $t->smallInteger('level');
            $t->string('code', 16);
            $t->string('descr', 32);
            $t->timestamps();

            $t->index('level');
            $t->index('code');
        });
    }
};
