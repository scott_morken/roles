<?php

namespace Database\Factories\Smorken\Roles\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;

class RoleUserFactory extends Factory
{
    protected $model = RoleUser::class;

    public function definition(): array
    {
        return [
            'role_id' => Role::factory(),
            'user_id' => $this->faker->randomNumber(2),
        ];
    }
}
