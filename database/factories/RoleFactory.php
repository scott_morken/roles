<?php

namespace Database\Factories\Smorken\Roles\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Support\Str;

class RoleFactory extends Factory
{
    protected $model = Role::class;

    public function definition(): array
    {
        return [
            'code' => Str::random(10),
            'descr' => $this->faker->words(3, true),
            'level' => $this->faker->randomNumber(3),
        ];
    }
}
