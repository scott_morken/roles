<?php

namespace Database\Seeders\Smorken\Roles\Models\Eloquent;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Database\Seeder;
use Smorken\Model\Contracts\Model;
use Smorken\Roles\Contracts\Models\Role;

class RoleSeeder extends Seeder
{
    protected ?Repository $config = null;

    protected mixed $model = null;

    protected string $modelClass = Role::class;

    public function run(): void
    {
        $this->truncate();
        foreach ($this->getData() as $row) {
            $this->create($row);
        }
    }

    protected function create(array $attributes): Model
    {
        return $this->getModel()
            ->create($attributes);
    }

    protected function getConfig(): Repository
    {
        if (! $this->config) {
            $this->config = $this->container->make(Repository::class);
        }

        return $this->config;
    }

    protected function getData(): array
    {
        return $this->getConfig()->get('sm-roles.allowed', []);
    }

    protected function getModel(): Model
    {
        if (! $this->model) {
            $this->model = $this->container->make($this->modelClass);
        }

        return $this->model;
    }

    protected function truncate(): mixed
    {
        return $this->getModel()
            ->truncate();
    }
}
