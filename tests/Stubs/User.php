<?php

namespace Tests\Smorken\Roles\Stubs;

use Illuminate\Contracts\Auth\Authenticatable;

class User implements Authenticatable
{
    public function __construct(public int $id = 1, public string $name = 'User') {}

    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return 'password';
    }

    public function getRememberToken()
    {
        return 'token';
    }

    public function setRememberToken($value)
    {
        // do nothing
    }

    public function getRememberTokenName()
    {
        // do nothing
    }

    public function getAuthPasswordName()
    {
        return 'password';
    }
}
