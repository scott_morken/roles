<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Feature\Http\Controllers;

use Illuminate\Auth\GenericUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Orchestra\Testbench\Attributes\DefineEnvironment;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\Smorken\Roles\TestCase;

class RoleUserControllerTest extends TestCase
{
    use RefreshDatabase;

    protected bool $seed = true;

    #[Test]
    #[DefineEnvironment('usesSqliteConnection')]
    public function it_allows_an_admin_user(): void
    {
        $user = new GenericUser(['id' => 1]);
        RoleUser::factory()->create(['role_id' => 1, 'user_id' => 1]);
        $this->actingAs($user);
        $this->get('/admin/role-user')->assertSuccessful();
    }

    #[Test]
    #[DefineEnvironment('usesSqliteConnection')]
    public function it_does_not_allow_non_admin_user(): void
    {
        $user = new GenericUser(['id' => 99]);
        $this->actingAs($user);
        $this->get('/admin/role-user')->assertForbidden();
    }

    #[Test]
    #[DefineEnvironment('usesDummyLoginRoute')]
    public function it_does_not_allow_unauthenticated(): void
    {
        $this->get('/admin/role-user')->assertRedirect('/login');
    }

    #[Test]
    #[DefineEnvironment('usesSqliteConnection')]
    public function it_shows_roles(): void
    {
        $user = new GenericUser(['id' => 1]);
        RoleUser::factory()->create(['role_id' => 1, 'user_id' => 1]);
        $this->actingAs($user);
        $this->get('/admin/role-user')
            ->assertSee('<td >1</td>', false);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->usesGates($this->app);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->resetGates();
    }
}
