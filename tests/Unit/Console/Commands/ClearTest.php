<?php

namespace Tests\Smorken\Roles\Unit\Console\Commands;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Roles\Actions\DeleteRoleUserByUserIdAction;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Symfony\Component\Console\Exception\RuntimeException;
use Tests\Smorken\Roles\TestCase;

class ClearTest extends TestCase
{
    #[Test]
    public function it_fails_when_no_users_are_removed(): void
    {
        $model = m::mock(new RoleUser)->makePartial();
        $this->bindDeleteAction($model);
        $query = m::mock(Builder::class);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->userIdIs(2)->andReturn($query);
        $query->expects()->delete()->andReturn(false);
        $this->artisan('role:clear', ['user_id' => 2])
            ->expectsOutput('User [2] does not exist or was not removed.');
        $this->assertTrue(true); // artisan doesn't trigger assertions check
    }

    #[Test]
    public function it_fails_without_a_user_id(): void
    {
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Not enough arguments (missing: "user_id")');
        $this->artisan('role:clear');
    }

    #[Test]
    public function it_successfully_removes_users(): void
    {
        $model = m::mock(new RoleUser)->makePartial();
        $this->bindDeleteAction($model);
        $query = m::mock(Builder::class);
        $model->expects()->newQuery()->andReturn($query);
        $query->expects()->userIdIs(2)->andReturn($query);
        $query->expects()->delete()->andReturn(true);
        $this->artisan('role:clear', ['user_id' => 2])
            ->expectsOutput('User [2] has been removed from the access table.');
        $this->assertTrue(true); // artisan doesn't trigger assertions check
    }

    protected function bindDeleteAction(\Smorken\Roles\Contracts\Models\RoleUser $model): void
    {
        $this->app[\Smorken\Roles\Contracts\Actions\DeleteRoleUserByUserIdAction::class] = new DeleteRoleUserByUserIdAction($model);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
