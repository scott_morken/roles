<?php

namespace Tests\Smorken\Roles\Unit\Console\Commands;

use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use Smorken\Roles\Contracts\Actions\AddRoleUserForUserIdAction;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\Smorken\Roles\TestCase;

class SetTest extends TestCase
{
    #[Test]
    public function it_adds_a_user_to_a_role_with_a_code_set(): void
    {
        $action = m::mock(AddRoleUserForUserIdAction::class);
        $this->app[AddRoleUserForUserIdAction::class] = $action;
        $userRole = new RoleUser;
        $role = new Role(['descr' => 'Some Role']);
        $userRole->setRelation('role', $role);
        $action->expects()->__invoke(2, 'manage')->andReturn($userRole);
        $this->artisan('role:set', ['user_id' => 2, 'role' => 'manage'])
            ->expectsOutput("User [2] added to role 'Some Role'.");
    }

    #[Test]
    public function it_adds_a_user_to_a_role_without_a_code_set(): void
    {
        $action = m::mock(AddRoleUserForUserIdAction::class);
        $this->app[AddRoleUserForUserIdAction::class] = $action;
        $userRole = new RoleUser;
        $role = new Role(['descr' => 'Some Role']);
        $userRole->setRelation('role', $role);
        $action->expects()->__invoke(2, null)->andReturn($userRole);
        $this->artisan('role:set', ['user_id' => 2])
            ->expectsOutput("User [2] added to role 'Some Role'.");
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
