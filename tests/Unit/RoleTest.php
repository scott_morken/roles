<?php

namespace Tests\Smorken\Roles\Unit;

use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Roles\Exception;
use Smorken\Roles\Factories\RoleRepositoryFactory;
use Smorken\Roles\Factories\RoleUserRepositoryFactory;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\Smorken\Roles\Concerns\WithRolesRepository;

class RoleTest extends TestCase
{
    use WithRolesRepository;

    protected ?RoleRepositoryFactory $roleRepositoryFactory = null;

    protected ?RoleUserRepositoryFactory $roleUserRepositoryFactory = null;

    #[Test]
    public function it_has_a_role(): void
    {
        $role = (new Role)->forceFill(['id' => 3, 'code' => 'foo', 'level' => 300]);
        $userRole = new RoleUser;
        $userRole->setRelation('role', $role);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn($userRole);
        $sut = $this->getSut();
        $this->assertTrue($sut->has($role, 2));
    }

    #[Test]
    public function it_returns_a_collection_of_all(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $sut = $this->getSut();
        $all = $sut->all();
        $this->assertEquals([
            'super-admin',
            'admin',
            'manage',
            'user',
            'none',
        ], $all->keys()->toArray());
    }

    #[Test]
    public function it_returns_max_level(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $sut = $this->getSut();
        $max = $sut->max();
        $this->assertEquals('super-admin', $max->code);
    }

    #[Test]
    public function it_returns_false_when_a_user_has_a_role_less_than_the_requested_code(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $role = (new Role)->forceFill(['id' => 10, 'code' => 'user', 'level' => 100]);
        $userRole = new RoleUser;
        $userRole->setRelation('role', $role);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn($userRole);
        $sut = $this->getSut();
        $this->assertFalse($sut->hasCode('manage', 2));
    }

    #[Test]
    public function it_returns_false_when_a_user_has_a_role_less_than_the_requested_level(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $role = (new Role)->forceFill(['id' => 1, 'code' => 'super-admin', 'level' => 100]);
        $userRole = new RoleUser;
        $userRole->setRelation('role', $role);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn($userRole);
        $sut = $this->getSut();
        $this->assertFalse($sut->hasLevel(900, 2));
    }

    #[Test]
    public function it_returns_true_when_a_user_has_a_role_greater_than_the_requested_code(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $role = (new Role)->forceFill(['id' => 1, 'code' => 'super-admin', 'level' => 1000]);
        $userRole = new RoleUser;
        $userRole->setRelation('role', $role);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn($userRole);
        $sut = $this->getSut();
        $this->assertTrue($sut->hasCode('admin', 2));
    }

    #[Test]
    public function it_returns_true_when_a_user_has_a_role_greater_than_the_requested_id(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $role = (new Role)->forceFill(['id' => 1, 'code' => 'super-admin', 'level' => 1000]);
        $userRole = new RoleUser;
        $userRole->setRelation('role', $role);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn($userRole);
        $sut = $this->getSut();
        $this->assertTrue($sut->hasId(3, 2));
    }

    #[Test]
    public function it_returns_true_when_a_user_has_a_role_greater_than_the_requested_level(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $role = (new Role)->forceFill(['id' => 1, 'code' => 'super-admin', 'level' => 1000]);
        $userRole = new RoleUser;
        $userRole->setRelation('role', $role);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn($userRole);
        $sut = $this->getSut();
        $this->assertTrue($sut->hasLevel(900, 2));
    }

    #[Test]
    public function it_returns_true_when_a_user_has_a_role_less_than_the_requested_id(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $role = (new Role)->forceFill(['id' => 5, 'code' => 'foo', 'level' => 50]);
        $userRole = new RoleUser;
        $userRole->setRelation('role', $role);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn($userRole);
        $sut = $this->getSut();
        $this->assertFalse($sut->hasId(2, 2));
    }

    #[Test]
    public function it_throws_an_exception_when_the_user_id_changes(): void
    {
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn(null);
        $sut = $this->getSut();
        $sut->currentRole(2);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("User ID changed from '2' to '3'");
        $sut->currentRole(3);
    }

    #[Test]
    public function it_throws_an_exception_without_a_role_matching_a_code(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn(null);
        $sut = $this->getSut();
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Role not found for 'fizz-buzz'");
        $sut->hasCode('fizz-buzz', 2);
    }

    #[Test]
    public function it_throws_an_exception_without_a_role_matching_a_level(): void
    {
        $c = $this->getRolesCollection();
        $this->getRoleRepositoryFactory()->expects()->all()->andReturns($c);
        $this->getRoleUserRepositoryFactory()->expects()->findByUserId(2)->andReturn(null);
        $sut = $this->getSut();
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Role not found for '9999'");
        $sut->hasLevel(9999, 2);
    }

    protected function getRoleRepositoryFactory(): RoleRepositoryFactory
    {
        if ($this->roleRepositoryFactory == null) {
            $this->roleRepositoryFactory = m::mock(RoleRepositoryFactory::class);
        }

        return $this->roleRepositoryFactory;
    }

    protected function getRoleUserRepositoryFactory(): RoleUserRepositoryFactory
    {
        if ($this->roleUserRepositoryFactory == null) {
            $this->roleUserRepositoryFactory = m::mock(RoleUserRepositoryFactory::class);
            $this->roleUserRepositoryFactory->expects()->setAuthorizeForFind(false);
        }

        return $this->roleUserRepositoryFactory;
    }

    protected function getSut(): \Smorken\Roles\Contracts\Role
    {
        return new \Smorken\Roles\Role($this->getRoleRepositoryFactory(), $this->getRoleUserRepositoryFactory());
    }

    protected function tearDown(): void
    {
        m::close();
    }
}
