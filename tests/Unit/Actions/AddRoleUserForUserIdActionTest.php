<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Unit\Actions;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Roles\Contracts\Actions\AddRoleUserForUserIdAction;
use Smorken\Roles\Factories\RoleRepositoryFactory;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\Smorken\Roles\Concerns\WithMockConnection;

class AddRoleUserForUserIdActionTest extends TestCase
{
    use WithMockConnection;

    protected ?RoleRepositoryFactory $roleRepositoryFactory = null;

    #[Test]
    public function it_adds_role_for_user_id_using_code(): void
    {
        $model = m::mock(new RoleUser)->makePartial();
        $role = (new Role)->forceFill(['id' => 3, 'code' => 'manage', 'level' => 500]);
        $this->getRoleRepositoryFactory()->expects()->findByCode('manage')->andReturns($role);
        $query = m::mock(Builder::class);
        $model->allows()->newQuery()->andReturns($query);
        $query->expects()->userIdIs(12345)->andReturnSelf();
        $query->expects()->first()->andReturns(null);
        $newModel = m::mock(new RoleUser)->makePartial();
        $model->expects()->newInstance(['user_id' => 12345, 'role_id' => 3])->andReturns($newModel);
        $newModel->expects()->save()->andReturns(true);
        $sut = $this->getSut($model);
        $roleUser = $sut(12345, 'manage');
        $this->assertSame($newModel, $roleUser);
    }

    #[Test]
    public function it_adds_role_for_user_id_using_max_role(): void
    {
        $model = m::mock(new RoleUser)->makePartial();
        $role = (new Role)->forceFill(['id' => 1, 'code' => 'super-admin', 'level' => 1000]);
        $this->getRoleRepositoryFactory()->expects()->max()->andReturns($role);
        $query = m::mock(Builder::class);
        $model->allows()->newQuery()->andReturns($query);
        $query->expects()->userIdIs(12345)->andReturnSelf();
        $query->expects()->first()->andReturns(null);
        $newModel = m::mock(new RoleUser)->makePartial();
        $model->expects()->newInstance(['user_id' => 12345, 'role_id' => 1])->andReturns($newModel);
        $newModel->expects()->save()->andReturns(true);
        $sut = $this->getSut($model);
        $roleUser = $sut(12345);
        $this->assertSame($newModel, $roleUser);
    }

    protected function getRoleRepositoryFactory(): RoleRepositoryFactory
    {
        if ($this->roleRepositoryFactory === null) {
            $this->roleRepositoryFactory = m::mock(RoleRepositoryFactory::class);
        }

        return $this->roleRepositoryFactory;
    }

    protected function getSut(\Smorken\Roles\Contracts\Models\RoleUser $model): AddRoleUserForUserIdAction
    {
        return new \Smorken\Roles\Actions\AddRoleUserForUserIdAction(
            $model,
            $this->getRoleRepositoryFactory()
        );
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
