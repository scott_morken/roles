<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Unit\Actions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Container\Container;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Contracts\Validation\Factory;
use Illuminate\Translation\ArrayLoader;
use Illuminate\Translation\Translator;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Domain\Actions\EloquentUpsertAction;
use Smorken\Domain\Actions\Upsertable;
use Smorken\Roles\Contracts\Actions\UpsertRoleUserAction;
use Smorken\Roles\Contracts\Repositories\FindRoleRepository;
use Smorken\Roles\Models\Eloquent\Role;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Smorken\Roles\Models\Eloquent\UserStub;
use Smorken\Roles\Policies\RoleUserPolicy;
use Tests\Smorken\Roles\Concerns\WithMockConnection;

class UpsertRoleUserActionTest extends TestCase
{
    use WithMockConnection;

    protected ?\Closure $authUserCallback = null;

    protected ?FindRoleRepository $findRoleRepository = null;

    protected ?Gate $gate = null;

    #[Test]
    public function it_fails_authorization_when_user_level_less_than_requested_during_create(): void
    {
        $model = m::mock(new RoleUser)->makePartial();
        $model->allows()->newInstance()->andReturnSelf();
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturns($model);
        $model->allows()->newQuery()->andReturns($query);
        $query->expects()->userIdIs(99)->andReturnSelf();
        $query->expects()->first()->andReturns(null);
        $role = (new Role)->forceFill(['id' => 1, 'level' => 1000, 'code' => 'super-admin']);
        $this->getFindRoleRepository()->expects()->__invoke(1)->andReturns($role);
        $sut = $this->getSut($model);
        $upsertable = new Upsertable(['role_id' => 1], ['user_id' => 99]);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have permission to create a new record');
        $r = $sut($upsertable);
    }

    #[Test]
    public function it_passes_authorization_when_user_level_gte_requested_during_create(): void
    {
        $model = m::mock(new RoleUser)->makePartial();
        $model->allows()->newInstance()->andReturnSelf();
        $query = m::mock(Builder::class);
        $query->allows()->getModel()->andReturns($model);
        $model->allows()->newQuery()->andReturns($query);
        $query->expects()->userIdIs(99)->andReturnSelf();
        $query->expects()->first()->andReturns(null);
        $role = (new Role)->forceFill(['id' => 1, 'level' => 900, 'code' => 'admin']);
        $this->getFindRoleRepository()->expects()->__invoke(1)->andReturns($role);
        $instanceOf = m::mock(new RoleUser(['user_id' => 99, 'role_id' => 1]))->makePartial();
        $model->expects()->newInstance(['user_id' => 99, 'role_id' => 1])->andReturn($instanceOf);
        $instanceOf->expects()->save()->andReturns(true);
        $sut = $this->getSut($model);
        $upsertable = new Upsertable(['role_id' => 1], ['user_id' => 99]);
        $r = $sut($upsertable);
        $this->assertSame($instanceOf, $r->model);
    }

    protected function getAuthUserCallback(): \Closure
    {
        if ($this->authUserCallback) {
            return $this->authUserCallback;
        }

        return function () {
            $user = new UserStub([
                'id' => 10,
            ]);
            $user->setRelation('role', new Role(['code' => 'admin', 'level' => 900]));

            return $user;
        };
    }

    protected function getFindRoleRepository(): FindRoleRepository
    {
        if (! $this->findRoleRepository) {
            $this->findRoleRepository = m::mock(FindRoleRepository::class);
        }

        return $this->findRoleRepository;
    }

    protected function getGate(): Gate
    {
        if (! $this->gate) {
            $this->initGate($this->getAuthUserCallback());
        }

        return $this->gate;
    }

    protected function getSut(\Smorken\Roles\Contracts\Models\RoleUser $model): UpsertRoleUserAction
    {
        $this->initGate($this->getAuthUserCallback());
        EloquentUpsertAction::setGate($this->getGate());
        EloquentUpsertAction::setValidationFactory($this->getValidationFactory());
        RoleUserPolicy::defineSelf(RoleUser::class, $this->getGate());
        RoleUserPolicy::defineSelf($model::class, $this->getGate());

        return new \Smorken\Roles\Actions\UpsertRoleUserAction($model);
    }

    protected function getValidationFactory(): Factory
    {
        return new \Illuminate\Validation\Factory(new Translator(new ArrayLoader, 'en'));
    }

    protected function initGate(\Closure $callback): void
    {
        $container = new Container;
        $container->bind(FindRoleRepository::class, fn () => $this->getFindRoleRepository());
        $this->gate = new \Illuminate\Auth\Access\Gate($container, $callback);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->initConnectionResolver();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        EloquentUpsertAction::clearGate();
    }
}
