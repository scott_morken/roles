<?php

namespace Tests\Smorken\Roles\Unit\Models\Eloquent;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Roles\Models\Eloquent\Role;

class RoleTest extends TestCase
{
    #[Test]
    public function it_slugs_code_attribute(): void
    {
        $sut = new Role(['code' => 'Foo BAR_/Biz']);
        $this->assertEquals('foo-bar-biz', $sut->code);
    }
}
