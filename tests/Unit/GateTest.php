<?php

namespace Tests\Smorken\Roles\Unit;

use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Smorken\Roles\Contracts\Role;
use Smorken\Roles\Support\DefineGates;
use Tests\Smorken\Roles\Stubs\User;

class GateTest extends TestCase
{
    protected ?Application $application = null;

    protected ?Dispatcher $dispatcher = null;

    #[Test]
    public function it_is_always_false_when_handler_all_is_exception(): void
    {
        $sut = $this->getSut();
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Gate::class)
            ->andReturn($sut);
        $roles = m::mock(Role::class);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Role::class)
            ->andReturn($roles);
        $roles->shouldReceive('all')
            ->once()
            ->andThrow(new \Exception);
        DefineGates::define($this->getApplication(), true);
        $this->assertFalse($sut->check('role-user'));
        $this->assertFalse($sut->check('role-manage'));
        $this->assertFalse($sut->check('role-admin'));
    }

    #[Test]
    public function it_is_always_false_when_making_handler_is_exception(): void
    {
        $sut = $this->getSut();
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Gate::class)
            ->andReturn($sut);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Role::class)
            ->andThrow(new \Exception);
        DefineGates::define($this->getApplication(), true);
        $this->assertFalse($sut->check('role-user'));
        $this->assertFalse($sut->check('role-manage'));
        $this->assertFalse($sut->check('role-admin'));
    }

    #[Test]
    public function it_is_always_false_without_a_user(): void
    {
        $sut = $this->getSut(fn () => null);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Gate::class)
            ->andReturn($sut);
        $roles = m::mock(Role::class);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Role::class)
            ->andReturn($roles);
        $roles->shouldReceive('all')
            ->once()
            ->andReturn(new Collection([
                (new \Smorken\Roles\Models\Eloquent\Role)->forceFill(['code' => 'user']),
                (new \Smorken\Roles\Models\Eloquent\Role)->forceFill(['code' => 'manage']),
                (new \Smorken\Roles\Models\Eloquent\Role)->forceFill(['code' => 'admin']),
            ]));
        $roles->shouldReceive('hasCode')
            ->never();
        $roles->shouldReceive('hasCode')
            ->never();
        $roles->shouldReceive('hasCode')
            ->never();
        DefineGates::define($this->getApplication(), true);
        $this->assertFalse($sut->check('role-user'));
        $this->assertFalse($sut->check('role-manage'));
        $this->assertFalse($sut->check('role-admin'));
    }

    #[Test]
    public function it_is_always_false_without_any_handlers(): void
    {
        $sut = $this->getSut();
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Gate::class)
            ->andReturn($sut);
        $roles = m::mock(Role::class);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Role::class)
            ->andReturn($roles);
        $roles->shouldReceive('all')
            ->once()
            ->andReturn(new Collection);
        DefineGates::define($this->getApplication(), true);
        $this->assertFalse($sut->check('role-user'));
        $this->assertFalse($sut->check('role-manage'));
        $this->assertFalse($sut->check('role-admin'));
    }

    #[Test]
    public function it_returns_true_for_a_valid_check(): void
    {
        $sut = $this->getSut();
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Gate::class)
            ->andReturn($sut);
        $roles = m::mock(Role::class);
        $this->getApplication()->shouldReceive('make')
            ->once()
            ->with(Role::class)
            ->andReturn($roles);
        $roles->shouldReceive('all')
            ->once()
            ->andReturn(new Collection([
                (new \Smorken\Roles\Models\Eloquent\Role)->forceFill(['code' => 'user']),
                (new \Smorken\Roles\Models\Eloquent\Role)->forceFill(['code' => 'manage']),
                (new \Smorken\Roles\Models\Eloquent\Role)->forceFill(['code' => 'admin']),
            ]));
        $roles->shouldReceive('hasCode')
            ->once()
            ->with('user', 2)
            ->andReturn(false);
        $roles->shouldReceive('hasCode')
            ->once()
            ->with('manage', 2)
            ->andReturn(false);
        $roles->shouldReceive('hasCode')
            ->once()
            ->with('admin', 2)
            ->andReturn(true);
        DefineGates::define($this->getApplication(), true);
        $this->assertFalse($sut->check('role-user'));
        $this->assertFalse($sut->check('role-manage'));
        $this->assertTrue($sut->check('role-admin'));
    }

    protected function getApplication(): Application|m\Mock
    {
        if ($this->application === null) {
            $this->application = m::mock(Application::class);
        }

        return $this->application;
    }

    protected function getDispatcher(): Dispatcher|m\Mock
    {
        if ($this->dispatcher === null) {
            $this->dispatcher = m::mock(Dispatcher::class);
        }

        return $this->dispatcher;
    }

    protected function getSut(?callable $userCallback = null): Gate
    {
        if ($userCallback === null) {
            $userCallback = fn () => new User(2);
        }
        $this->getApplication()->shouldReceive('bound')
            ->with(Dispatcher::class)
            ->andReturn(false);

        //        $this->getApplication()->shouldReceive('make')
        //            ->with(Dispatcher::class)
        //            ->andReturn($this->getDispatcher());
        return new \Illuminate\Auth\Access\Gate($this->getApplication(), $userCallback);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        DefineGates::reset(true);
    }
}
