<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles;

use Database\Seeders\Smorken\Roles\Models\Eloquent\RoleSeeder;
use Illuminate\Support\Facades\Route;
use Smorken\CacheAssist\ServiceProvider;
use Smorken\Domain\ActionServiceProvider;
use Smorken\Domain\DomainServiceProvider;
use Smorken\Domain\RepositoryServiceProvider;
use Smorken\Roles\Support\DefineGates;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected string $seeder = RoleSeeder::class;

    protected function defineEnvironment($app): void
    {
        $app['config']->set('sm-roles.load_routes', true);
        $app['config']->set('sm-roles.layout', 'sm-roles::layout');
    }

    protected function getPackageProviders($app): array
    {
        return [
            ServiceProvider::class,
            \Smorken\Components\ServiceProvider::class,
            \Smorken\Roles\ServiceProvider::class,
            DomainServiceProvider::class,
            ActionServiceProvider::class,
            RepositoryServiceProvider::class,
        ];
    }

    protected function resetGates(): void
    {
        DefineGates::reset(true);
    }

    protected function usesDummyLoginRoute($app): void
    {
        Route::get('login', function () {
            return 'login';
        })->name('login');
    }

    protected function usesGates($app): void
    {
        DefineGates::define($app, true);
    }

    protected function usesSqliteConnection($app): void
    {
        touch(database_path('database.sqlite'));
        $app['config']->set('database.default', 'testing');
        $app['config']->set('database.connections.testing', [
            'driver' => 'sqlite',
            'database' => ':memory:',
        ]);
    }
}
