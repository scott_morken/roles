<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Concerns;

use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\Roles\Constants\RoleName;
use Smorken\Roles\Contracts\Repositories\RolesRepository;
use Smorken\Roles\Models\Eloquent\Role;

trait WithRolesRepository
{
    protected RolesRepository|m\MockInterface|null $rolesRepository = null;

    protected function getRolesCollection(): Collection
    {
        return new Collection([
            (new Role)->forceFill([
                'id' => 1,
                'code' => RoleName::SUPER_ADMIN->value,
                'descr' => RoleName::SUPER_ADMIN->label(),
                'level' => 1000,
            ]),
            (new Role)->forceFill([
                'id' => 2,
                'code' => RoleName::ADMIN->value,
                'descr' => RoleName::ADMIN->label(),
                'level' => 900,
            ]),
            (new Role)->forceFill([
                'id' => 3,
                'code' => RoleName::MANAGE->value,
                'descr' => RoleName::MANAGE->label(),
                'level' => 800,
            ]),
            (new Role)->forceFill([
                'id' => 4,
                'code' => RoleName::USER->value,
                'descr' => RoleName::USER->label(),
                'level' => 100,
            ]),
            (new Role)->forceFill([
                'id' => 5,
                'code' => RoleName::NONE->value,
                'descr' => RoleName::NONE->label(),
                'level' => 0,
            ]),
        ]);
    }

    protected function getRolesRepository(): RolesRepository|m\MockInterface
    {
        if (! $this->rolesRepository) {
            $this->rolesRepository = m::mock(RolesRepository::class);
        }

        return $this->rolesRepository;
    }

    protected function mockAllRoles(): void
    {
        $this->getRolesRepository()->allows()->__invoke()->andReturn($this->getRolesCollection());
    }
}
