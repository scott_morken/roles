<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Concerns;

use Smorken\Roles\Contracts\Repositories\FindRoleByCodeRepository;
use Smorken\Roles\Contracts\Repositories\RolesRepository;
use Smorken\Roles\Factories\RoleRepositoryFactory;

trait WithRoleRepositoryFactory
{
    use WithFindRoleByCodeRepository, WithRolesRepository;

    protected ?RoleRepositoryFactory $roleRepositoryFactory = null;

    protected function getRoleRepositoryFactory(): RoleRepositoryFactory
    {
        if ($this->roleRepositoryFactory === null) {
            $this->roleRepositoryFactory = new RoleRepositoryFactory([
                RolesRepository::class => $this->getRolesRepository(),
                FindRoleByCodeRepository::class => $this->getFindRoleByCodeRepository(),
            ]);
        }

        return $this->roleRepositoryFactory;
    }
}
