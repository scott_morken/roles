<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Concerns;

use Mockery as m;
use Smorken\Roles\Contracts\Repositories\FindRoleByCodeRepository;

trait WithFindRoleByCodeRepository
{
    use WithRolesRepository;

    protected FindRoleByCodeRepository|m\MockInterface|null $findRoleByCodeRepository = null;

    protected function getFindRoleByCodeRepository(): FindRoleByCodeRepository|m\MockInterface
    {
        if (! $this->findRoleByCodeRepository) {
            $this->findRoleByCodeRepository = m::mock(FindRoleByCodeRepository::class);
        }

        return $this->findRoleByCodeRepository;
    }

    protected function mockAllRolesForFindRoleByCodeRepository(): void
    {
        foreach ($this->getRolesCollection() as $role) {
            $this->getFindRoleByCodeRepository()->allows()->__invoke($role->code, m::type('boolean'))->andReturn($role);
        }
    }
}
