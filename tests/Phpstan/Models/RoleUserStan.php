<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Phpstan\Models;

use Smorken\Roles\Models\Builders\RoleUserBuilder;
use Smorken\Roles\Models\Eloquent\RoleUser;

class RoleUserStan extends RoleUser {}

$m = new RoleUserStan;
assert($m->newQuery()->defaultOrder()->userIdIs(1) instanceof RoleUserBuilder);
