<?php

declare(strict_types=1);

namespace Tests\Smorken\Roles\Phpstan\Models;

use Smorken\Roles\Models\Builders\RoleBuilder;
use Smorken\Roles\Models\Eloquent\Role;

class RoleStan extends Role {}

$m = new RoleStan;
assert($m->newQuery()->defaultOrder()->greaterThanEqual(1) instanceof RoleBuilder);
